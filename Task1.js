// Question 1

function multiply(a, b) {
    return a*b;
}

// Question 2

function addition(a, b, c) {
    return a+b+c;
}

// Question 3

function division(a) {
    return a/2;
    
}

// Question 4

function area(a, b) {
    let c=5;
    return c*(a+b);
    
}

// Illustrate using code how the block-scoped variables work-----------

// Values assigned to a variable inside a block (loop or function) cannot appear in the console log 
// ( hence override existing values assigned outside the block) in case a value had been already assigned outside the block and the console.log
// command is placed outside the block. 

var name1="Mihalis";

let age="undefined";

function giveme () {
    let age="29";
    clear(name1);
    var name1="Babis";

}

console.log(age);
console.log(name1);


// Illustrate using code how hoisting works-------------

// Hoisting is a two-step process of assigning values to variables. Phase one, context creationg goes throught the code
// and assigns memory space to variables and subsequently when the code is executed values are assigned to the variables.
// It allows positioning of code in any position as only the execution is performed from top to bottom. 

hoist(); 

function hoist() {
  console.log('This function has been hoisted.');
};



// In code use an example of coersion using different data types-------------

const wololo= 1 + "2";
let outrageous= 2 - "1";
const compareme= 2>'1';
const thong=1+ "don't"+" "+"see"+" "+"a"+" "+"thing";
const weird=(1=="1") && (2==='2');


//  Write an employee function that can be invoked using the new keyword


function Employee(number, name, surname, email) {
    this.number=number;
    this.name= name;
    this.surname=surname;
    this.email=email;
    this.fullName = function(){
        return this.name+" "+this.surname;
    };

    this.id = function(){
        return this.number+","+this.fullName ();
    };
}

let mihalis = new Employee("1743", "Mihalis","Korres","asdgasgad@gmail.com");

// Create an object that represents an object

const Javascript = {
    aka:"JS",
    type: "programming language",
    age: "Can't remember",
    mentalbreakdownprobability: "distinct",
    size: {
        bust:90,
        waist:120,
        hips:150
    },
    getFullName: function() {
        console.log(this);
    }
}